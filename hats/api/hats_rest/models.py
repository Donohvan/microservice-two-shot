from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class Hat(models.Model):
    fabric = models.CharField(max_length=200, blank=True)
    style_name = models.CharField(max_length=200, blank=True)
    color = models.CharField(max_length=200, blank=True)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True,
    )

    def get_api_url(self):
        return reverse("api_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return self.style_name

    class Meta:
        ordering = ("style_name",)
