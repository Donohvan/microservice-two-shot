from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "import_href"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",

    ]
    encoders = {
        "location": LocationVODetailEncoder
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",

    ]
    encoders = {
        "location": LocationVODetailEncoder
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = f'/api/locations/{content["location"]}/'
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

# @require_http_methods(["DELETE", "GET", "PUT"])
# def api_show_hats(request, pk):
#     """
#     Returns the details for the Location model specified
#     by the pk parameter.

#     This should return a dictionary with the name, city,
#     room count, created, updated, and state abbreviation.

#     {
#         "name": location's name,
#         "city": location's city,
#         "room_count": the number of rooms available,
#         "created": the date/time when the record was created,
#         "updated": the date/time when the record was updated,
#         "state": the two-letter abbreviation for the state,
#     }
#     """
#     if request.method == "GET":
#         hat = Hat.objects.get(id=pk)
#         return JsonResponse(
#             hat,
#             encoder=HatDetailEncoder,
#             safe=False,
#         )
#     elif request.method == "DELETE":
#         count, _ = Hat.objects.filter(id=pk).delete()
#         return JsonResponse({"deleted": count > 0})
#     else:
#         content = json.loads(request.body)
#         try:
#             if "location" in content:
#                 location = Location.objects.get(abbreviation=content["location"])
#                 content["location"] = location
#         except Location.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid "},
#                 status=400,
#             )
#         Hat.objects.filter(id=pk).update(**content)
#         hat = Hat.objects.get(id=pk)
#         return JsonResponse(
#             hat,
#             encoder=HatDetailEncoder,
#             safe=False,
#         )
