# Generated by Django 4.0.3 on 2023-04-20 14:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hat',
            options={'ordering': ('style_name',)},
        ),
        migrations.RemoveField(
            model_name='hat',
            name='name',
        ),
        migrations.AddField(
            model_name='hat',
            name='color',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='hat',
            name='fabric',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='hat',
            name='location',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hats', to='hats_rest.locationvo'),
        ),
        migrations.AddField(
            model_name='hat',
            name='picture_url',
            field=models.URLField(null=True),
        ),
        migrations.AddField(
            model_name='hat',
            name='style_name',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
