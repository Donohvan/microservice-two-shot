from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Shoes, BinVO


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["bin_number",
                  "import_href"
                  ]


class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = ["name",
                  "bin",
                  "manufacturer",
                  "color",
                  "picture_url"]

    def get_extra_data(self, o):
        return {"bin": o.bin.bin_number}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
           {"shoes": shoes},
           encoder=ShoeListEncoder,
           safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = f'/api/bins/{content["bin"]}/'
            print(bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {'message': "Invalid bin id"},
                status=400,
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, id):
    if request.method == "GET":
        try:
            shoe = Shoes.objects.get(id=id)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoes.DoesNotExist:
            return JsonResponse(
                {"message": "There is no shoe"}
            )
    elif request.method == "DELETE":
        try:
            count, _ = Shoes.objects.filter(id=id).delete()
            return JsonResponse({
                "deleted": count > 0
            })
        except Shoes.DoesNotExist:
            return JsonResponse({
                "error": "There never was a shoe"
            })
    else:
        content = json.loads(request.body)
        try:
            shoe = Shoes.objects.get(id=id)
            properties = [
                "manufacturer",
                "model_name",
                "color",
                "picture_url",
                "bin"
            ]
            for prop in properties:
                if prop in content:
                    setattr(shoe, prop, content[prop])
                shoe.save()
                return JsonResponse(
                    shoe,
                    encoder=ShoeDetailEncoder,
                    safe=False,
                )
        except Shoes.DoesNotExist:
            return JsonResponse({
                "error": "Aint no shoe"
                })
