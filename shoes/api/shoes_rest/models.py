from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    bin_number = models.IntegerField(default=True)
    

class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200, blank=True)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=150, blank=True)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        default=True,
    )

    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"id": self.id})