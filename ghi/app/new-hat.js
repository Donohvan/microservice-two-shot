window.addEventListener('DOMContentLoaded', async () => {



    const url = 'http://localhost:8090/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('locations');
      for (let location of data.locations){
        const option = document.createElement('option')
        option.value = location.id
        option.innerHTML = location.name
        selectTag.appendChild(option)
      }
    }

    const formTag = document.getElementById('create-hat-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      const hatUrl = 'http://localhost:8090/api/hats/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
      };
      const responses = await fetch(hatUrl, fetchConfig);
      if (responses.ok) {
        formTag.reset();
        const newHat = await responses.json();
        console.log(newHat);
      }
    });

  });
