import React, { useEffect, useState } from "react"

function ShoeList(){

  const[shoes, setShoes] = useState([])

  const fetchData = async ()=> {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    }
  }

  useEffect(() => {
    fetchData();
  }, [])

  return (
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map((shoes) => {
            return (
              <tr key={shoes.id} value={shoes.id}>
                <td>{shoes.manufacturer}</td>
                <td>{shoes.name}</td>
                <td>{shoes.color}</td>
                <td>{shoes.picture}
                  <img src={shoes.picture_url} width="75px" height="75px" />
                </td>
                <td>{shoes.bin}</td>

              </tr>
            );
          })}
        </tbody>
      </table>
      // <button onClick={() => this.delete(hat.id)}></button>
  );
  

}

export default ShoeList;